# Alerant test Nexus

## Feladatleírás (HU)

> Indíts el dockerben egy Sonatype Nexus konténert, a Nexusban konfigurálj be egy docker típusú repót, és egy usert, akinek van joga feltölteni ebbe a repóba, majd a host gépről pusholj fel az előbb bekonfigurált userrel egy tetszőleges docker image-et ebbe a repóba. Írj a lépésekről egy rövid dokumentációt, ami alapján reprodukálni lehet a lépéseket.<br />
> A feladat megoldására 3 óra áll a rendelkezésedre, így a megoldásod juttasd el nekem még a mai napon legkésőbb 20:00-ig.

## References:

* https://hub.docker.com/r/sonatype/nexus3
* https://gitlab.com/xsakos/alerant-test-nexus
* SSL:
  - https://help.sonatype.com/repomanager3/nexus-repository-administration/configuring-ssl
  - https://support.sonatype.com/hc/en-us/articles/217542177-Using-Self-Signed-Certificates-with-Nexus-Repository-Manager-and-Docker-Daemon
  - https://horochovec.dev/using-sonatype-nexus-repository-manager-with-https-for-docker-private-registry/
  - https://shashanksrivastava.medium.com/how-to-secure-sonatype-nexus-using-a-self-signed-certificate-use-it-as-a-secure-docker-registry-dbea20556300

# Deploy:

* Clone this repo
* Enter repo dir
* `echo "JKS_PASS={changeit}" > .env`
* startup command: `docker-compose up -d`

Please note, when stopping use `-t` or `--timeout` switch, like: `docker-compose stop --timeout 120`

To reach the web ui use `http://{servername}:8081` (or the port you specified in the `docker-compose.yml` file)

:exclamation: <span style="color:red;">TODO: access it via https reverse proxy!</span>
# Initial setup

To get the admin password:
* Find the name of the container: `docker ps -a | grep nexus3` (let's assume the container name is: `alerant-test-nexus-nexus-1`)
* Run this command to get the password: `docker exec -ti alerant-test-nexus-nexus-1 cat /nexus-data/admin.password`

Login with the username **admin** and the password from the output of the previous command.

On first login you get a setup wizard.
* Change admin password to a strong one.
* Configure anonymous access: choose "*Disable anonymous access*"

# Create docker repository

* Go to "Server administration and configuration" (the cogwheel at the top) >> "Repository" >> "Repositories"
* Hit "Create repository" button
* Choose "Docker (hosted)" option
* Enter a "Name"
* Enter **8083** as "*Repository Connector*" for `https`
* Hit "Create repository" button at the bottom
* Note the repo URL

# Create role

* Go to "Server administration and configuration" (the cogwheel at the top) >> "Security" >> "Roles"
* Give a nae
* Role privileges: 
    - nx-repository-admin-docker-{reponame}-browse
    - nx-repository-admin-docker-{reponame}-delete
    - nx-repository-admin-docker-{reponame}-edit
    - nx-repository-admin-docker-{reponame}-read
    - nx-repository-view-docker-{reponame}-browse
    - nx-repository-view-docker-{reponame}-delete
    - nx-repository-view-docker-{reponame}-edit
    - nx-repository-view-docker-{reponame}-read
    - nx-repository-view-docker-{reponame}-read
* Hit "Create Role" button
* Choose "Nexus role"

# Create user

* Go to "Server administration and configuration" (the cogwheel at the top) >> "Security" >> "User"
* Give a name for the user...
* Add user to the previously created role.

:zap: <span style="color: red; font-weight: bold; font-size:18pt;">From here I'm feeling running out of time so I write what I think I suppose to do...</span>

# Create SSL certificate

* Enter the docker container with `docker exec ...`
* Commands to create a keystore:
```shell
cd /nexus-data/keystores
tar cvfz backup.tgz .
keytool -genkeypair -keystore rekettye.jks -storepass changeit -keypass changeit -alias rekettye -keyalg RSA -keysize 2048 -validity 5000 -dname "CN=servername.domain.tld, OU=Rekettye, O=Sonatype, L=Budapest, ST=Budapest, C=HU" -ext "SAN=DNS:servername.domain.tld,IP:192.168.1.220" -ext "BC=ca:true"
```
* Create new file: `/nexus-data/etc/rekettye/rekettye-https.xml`
```xml
<Set name="KeyStorePath"><Property name="ssl.etc"/>/rekettye.jks</Set>
<Set name="KeyStorePassword">changeit</Set>
<Set name="KeyManagerPassword">changeit</Set>
<Set name="TrustStorePassword">changeit</Set>
```

:boom: Aaaaaaaaaaaaaaaaaaaa...

# Build image
* `cd test-image/`
* `docker build -t rekettye:latest .`

# Upload image to repo
* To use self signed certs docker daemon may need an insecure registry entry on the client:
```json
{
	"insecure-registries": ["{servername}:8083"],
	"disable-legacy-registry": true
}
```
* restart docker
* :zap: **WARNING:** This setup won't work from the server where Nexus runs. Host networking should be used instead!
* `export todocker=http://{servername}:8083/repository/{reponame}}/`
* `docker login --username {username} $todocker`
* `docker push $todocker`
